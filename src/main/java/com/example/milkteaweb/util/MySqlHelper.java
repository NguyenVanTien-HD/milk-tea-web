package com.example.milkteaweb.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MySqlHelper {

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        String hostName = "localhost";
        String dbName = "milk_tea";
        String userName = "root";
        String password = "Tt123456789@";

        Class.forName("com.mysql.cj.jdbc.Driver");
        return DriverManager.getConnection("jdbc:mysql://" + hostName + ":3306/" + dbName + "?useSSL=false", userName, password);
    }
}
