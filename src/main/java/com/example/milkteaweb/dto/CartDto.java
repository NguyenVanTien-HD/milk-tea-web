package com.example.milkteaweb.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class CartDto implements Serializable {
    private List<CartItemDto> items = new ArrayList<>();
    private String address;
    private String receiver;
    private double totalPrice;
    private String phone;
    private double shipFee;

    public CartDto() {
        this.items = new ArrayList<>();
        this.address = "";
        this.receiver = "";
        this.totalPrice = 0;
        this.phone = "";
        this.shipFee = 0;
    }
}
