package com.example.milkteaweb.dto;

import com.example.milkteaweb.entity.Category;
import com.example.milkteaweb.entity.Product;
import lombok.Data;

import java.io.Serializable;

@Data
public class CartItemDto implements Serializable {
    private Product product;
    private int quantity;
    private Category category;
}
