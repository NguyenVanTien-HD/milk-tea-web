package com.example.milkteaweb.entity;

import lombok.Data;

import java.util.UUID;

@Data
public class Orders {
    private String id;
    private String address;
    private String receiver;
    private double totalPrice;
    private String phone;
    private double shipFee;

    public Orders() {
        this.id = UUID.randomUUID().toString();
    }
}
