package com.example.milkteaweb.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class User {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String username;
    private String password;

    public User() {
        this.firstName = "";
        this.lastName = "";
        this.email = "";
        this.phone = "";
        this.username = "";
        this.password = "";
    }
}
