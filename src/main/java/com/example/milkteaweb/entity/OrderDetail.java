package com.example.milkteaweb.entity;

import lombok.Data;

@Data
public class OrderDetail {
    private int productId;
    private int quantity;
    private int categoryId;
    private String orderId;
}
